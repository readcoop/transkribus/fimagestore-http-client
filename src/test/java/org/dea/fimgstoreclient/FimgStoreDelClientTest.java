package org.dea.fimgstoreclient;
import org.dea.fimgstoreclient.AbstractHttpClient.Scheme;

public class FimgStoreDelClientTest {

	public static void main(String[] args) {
	
		String[] fileKeys = {"DACRGKYTEAKRPQUIQQSNLKJZ"};
		FimgStoreDelClient delClient = 
				new FimgStoreDelClient(Scheme.https, "files-test.transkribus.eu", "/", args[0], args[1]);
		
			for(String key : fileKeys){
				try{
					System.out.println("Delete key: " + key);
					System.out.println(delClient.deleteFile(key, 5));
				} catch (Exception e) {
					e.printStackTrace();
				}	
			}
			
	}
}