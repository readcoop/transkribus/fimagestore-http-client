package org.dea.fimgstoreclient;

import org.dea.fimgstoreclient.utils.FimgStoreUriBuilder;

public interface IFimgStoreClientBase {
	FimgStoreUriBuilder getUriBuilder();
	
	default boolean hasFileAccess() {
		return false;
	}
}
